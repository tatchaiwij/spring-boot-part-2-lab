package lab1;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@ComponentScan("lab1")
@PropertySource("classpath:application.properties")
public class DevConfig {

}
